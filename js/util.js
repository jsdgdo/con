jQuery(document).ready(function($) {
  //Menú functionality
  var nav = $('.js-nav');
  $('#mmt').click(function(event) {
    event.preventDefault();
    if ($(this).parent().hasClass('active'))
    {
      $(this).parent().removeClass('active');
      $(this).children('i').removeClass('fa-times').addClass('fa-bars');
      nav.slideToggle();
    }else{
      $(this).parent().addClass('active');
      $(this).children('i').removeClass('fa-bars').addClass('fa-times');
      nav.slideToggle();
    }
  });

  //Search functionality
  var hs = $('.header-search');
  $('#st').click(function(event) {
    event.preventDefault();
    if (hs.hasClass('active'))
    {
      hs.find('input').focusout();
      hs.removeClass('active');
    }else{
      hs.addClass('active').bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
        if (hs.hasClass('active'))
        {
          hs.find('input').focus();
        }
      });
    }
  });

  $(document).keyup(function(e) {
    if (e.keyCode === 27)
    {
      if (hs.hasClass('active'))
      {
        hs.removeClass('active');
        hs.find('input').focusout();
      }else{
        return;
      }
    }
  });

});
