jQuery(document).ready(function($) {
  //sidescroll
  //Créditos: https://css-tricks.com/scrollfollow-sidebar/

  var $sidebar   = $("#sidebar"),
      $footer    = $("footer"),
      $window    = $(window),
      offset     = $sidebar.offset(),
      foffset    = $footer.offset(),
      topPadding = 10;

  $window.scroll(function() {
    if ($window.outerWidth() > 799)
    {
      if ($window.scrollTop() > offset.top && $window.scrollTop() < foffset.top) {
          $sidebar.stop().animate({
              marginTop: $window.scrollTop() - offset.top + topPadding
          });
      } else {
          $sidebar.stop().animate({
              marginTop: 0
          });
      }
    }
  });

});
